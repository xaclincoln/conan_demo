
使用conan做c++的包管理器

## 安装Conan
pip install conan

## 安装和编译依赖
mkdir build && cd build
conan install .. -s build_type=Debug --build=fmt

## 使用CMAKE编译
cmake ..
cmake --build .

## 调试
ctrl+F5


这里没有将fmt库做成动态链接库，而是选择静态链接，主要原因是便于共享